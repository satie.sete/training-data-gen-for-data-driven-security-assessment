<p align="center">
  <img width="250" height="100" src="https://user-images.githubusercontent.com/73366653/97095321-45e05a00-165e-11eb-9c60-b90723caba4a.png">
  <img width="250" height="100" src="https://user-images.githubusercontent.com/73366653/97095369-b8513a00-165e-11eb-9ef2-e2fcef626f88.jpeg">
  <img width="150" height="100" src="https://user-images.githubusercontent.com/73366653/97095285-c0f54080-165d-11eb-82bf-e0c032a1e333.png">    
  <img width="110" height="110" src="https://user-images.githubusercontent.com/73366653/97095257-825f8600-165d-11eb-8704-c998a9fae1ce.png"> 
   
</p>

# Data generation strategies for training of data-driven security assessment classification methods for low voltage smart grids
This repository contains the notebooks to (1) generate different training datasets and testing, (2) train data driven models (decision trees, gradient tree boosting, and deep neural networks), and (3) to evaluate their performances.

- The studied SG is the standard IEEE European low voltage test network (ELVTN) with 55 customers.
- The security of the grid is assessed using OpenDSS as power flow simulator to label an operational point (OP) as "safe” if voltage at each node is not outside of a ± 0.05 p.u. band, and if the line Ampere limits of each line are respected, “unsafe” otherwise.
- 1 million points are generated for training using each strategy described below and to verify the effect of dataset size, four subsets are used for training with random permutations: 1,000, 10,000, 100,000 and 1 million OPs.
- This approach is ran 10 times to report on average performances.
- The repository uses approximately 10 MB of disk space, once the master notebooks are run, this goes up to approximately 35 GB of disk space.

This code is associated to the conference paper :
"Training Data Generation Strategies for Data-driven Security Assessment of Low Voltage Smart Grids"
J. Cuenca, E. Aldea, E. Le Guern-Dall'o, R. Féraud, G. Camilleri, and A. Blavette.
Under review for the ISGT EU 2024 Conference.

 ___
### Files 
Three master jupyter notebooks are provided:

- (n1) "1_Master_DataGen.ipynb" to call other notebooks (d2) that generate and store training and testing datasets, including labels. 
- (n2) "2_Master_ModelBenchmark.ipynb" to call other notebooks to train and test the models using the generated datasets and subsets. Each training cycle is performed 10 times.
- (n3) "3_Master8ResultAnalyses.ipynb" to summarize results and generate the graphics.

 ___
### Directories
Five directories are provided:

- (d1) "/European_LV_DSS/" includes the electrical model used by OpenDSS to run the PF simulations that label OPs
- (d2) "/DataGen/" includes the notebooks for different data generation strategies and data augmentation.
- (d3) "/Datasets/" is where the generated datasets for training and testing are stored.
- (d4) "/ModelBenchmark/" includes the notebooks to train different data-driven models: decision trees, gradient tree boosting and deep neural networks.
- (d5) "/Results/" is where the trained models and results are stored in subdirectories with the following structure: "Results/DatasetSize/RunNumber/DataDrivenMethod/DataGenStrategyUsed/..."
 ___
### Data Generation Strategies
The performance of data-driven methods is largely influenced by an appropriate training dataset. In the absence of real-life information, it is necessary to generate synthetic datasets and to label them as "safe" or "unsafe" before training can start. We put forward different alternatives for the problem of security assessment applied to low and medium voltage SGs.

## a) Random data generation
This strategy produces randomly generated operational scenarios between two margins associated to the base loading of the SG. The selection of the upper bound greatly limits the proportion of safe and unsafe scenarios (i.e., if the upper bound is too small, all scenarios will be safe; if it is too large, all scenarios will be unsafe), thus creating a highly imbalanced training dataset whose influence on the algorithm performance can be extremely negative. Therefore, we argue that training using randomly generated data is not able to generalise for real life scenarios for medium to low voltage networks.

## b) Guided single random step 
We propose to explore iteratively a frontier of high-information content. This data generation strategy involves generating a first OP and labelling it with the PF simulator. If the OP is safe, one of the nodes is randomly selected to increase its load by a random quantity, making the next OP "less safe". If the OP is unsafe, its load is decreased by a random quantity, making the next OP "safer". By repeating this label-oriented process we aim at "guiding" the dataset towards having a balanced pool of safe and unsafe scenarios.

## c) Guided single fixed step
This data generation strategy is similar to that above, but instead of increasing/decreasing the load by a random step, a fixed step is predefined to discretise the search space. 

## d) Guided global random step
In this strategy, instead of selecting one random node at each iteration, the power consumption of all nodes is increased/decreased simultaneously by randomly generated quantities. 

## e) Guided single random step with multiple explorations
This strategy is equivalent to that in (b) but after a number of scenarios has been generated, a completely random OP restarts the exploration. This way several high information content frontiers can be explored.

## f) Guided single fixed step with multiple explorations
Following the logic from the previous strategy, we also propose to generate a dataset using a fixed step like in (c), but with multiple explorations in different frontiers.

## Data augmentation
To avoid biases towards a dominant category if the dataset is not balanced, it is proposed to use a state-of-the-art data augmentation strategy called Synthetic Minority Oversampling Technique (SMOTE), which increases the number of samples of the minority classification without adding new information to the model. All generated datasets will be augmented using SMOTE to also test the effects of balancing in model performance. 
 ___
## Contact
For any further information, you can contact at <juan.cuenca-silva@irit.fr>.
